<?php
include "inc/connection.php";
include "inc/function.php";
?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <!--CSS from Asset-->
  <link rel="stylesheet" href="asset/css/Style.css">

  <title>WARRIOR</title>
</head>
<body>
  
  <!-- background image -->
  <div class="fullscreen-bg">
    <img src="asset/image/bg_2.png" id="BackgroundImage" alt="not Found">
  </div>
  
  
  <!-- Navbar Header -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="navbar_header"  >
    <b class="navbar-brand Font1 text-light">Warrior</b>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <p class="nav-brand text-light Font1">Creare Your Own Character</p>
        </li>
      </ul>
    </div>
    
    <!-- create charater button into modal-->
    <button class="btn btn-link Font1 text-light mr-2" id="button_into_modal" data-toggle="modal" data-target="#modal-reng"><i class="far fa-id-badge Font1"></i> Create Char</button>
  </div>
  
  <!-- search -->
  <form class="form-inline my-2 my-lg-0" action="index.php" method="POST">
    <input class="form-control mr-sm-2 Font1" name="search" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-light my-2 my-sm-0 Font1" name="search_char" type="submit">Search</button>
  </form>
</nav>

<!-- modal -->
<div class="modal fade" id="modal-reng" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content bg-dark">
      <div class="modal-header bg-danger">
        
        <!--modal Form create character  -->
        <h5 class="modal-title Font1 text-light">ADD Character</h5>
      </div>
      <form class="Font1" action="index.php" method="POST">
        <div class="modal-body">
          
          <!-- input name -->
          <div class="form-group">
            <label for="inputName" class="text-light" >Name</label>
            <input type="text" class="form-control" id="name_char" aria-describedby="emailHelp" name="cre-name" required>
          </div> 
          
          <!-- select Role -->
          <div class="form-group">
            <label for="role option" class="text-light">Select Role</label>
            <select class="form-control" id="Select_role" name="cre-role">
              <option>Mage</option>
              <option>Archer</option>
              <option>Assasin</option>
              <option>Warrior</option>
              <option>Support</option>
              <option>Tank</option>
            </select>
          </div>
          
          <!-- select Weapon -->
          <div class="form-group">
            <label for="role option" class="text-light">Select Weapon</label>
            <select class="form-control" id="Select_weapon" name="cre-weapon">
              <option>Sword</option>
              <option>Bow</option>
              <option>Katana</option>
              <option>Staff</option>
              <option>Shield</option>
              <option>Knife</option>
            </select>
          </div>
          
          <!-- select gender -->
          <div class="form-group">
            <label for="role option" class="text-light">Select Role</label>
            <select class="form-control" id="Select_gender" name="cre-gender">
              <option>male</option>
              <option>Female</option>
            </select>
          </div>
        </div>
        
        <!-- submit -->
        <div class="modal-footer">
          <button type="button" class="btn btn-dark Font1" data-dismiss="modal">Cancle</button>
          <input type="submit" class="btn btn-dark Font1" name="sub-create" value="Create">
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Body Card -->
<div class="container">
  <div class="row">
    
    <!-- alert search -->
    <?php if(isset($error)): ?>
    <div class="alert alert-dismissible bg-dark text-light fade show Font1" style="z-index: 4;" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-contract" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M3.646 13.854a.5.5 0 0 0 .708 0L8 10.207l3.646 3.647a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 0 0 0 .708zm0-11.708a.5.5 0 0 1 .708 0L8 5.793l3.646-3.647a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 0-.708z"/>
        </svg>
      </button>
      <?php echo $error; ?>
    </div>
    <?php endif; ?>
    
    <!--empty text for end of line -->
    <div class="col-3">
      <h3 class="Font1 text-danger" style="margin-top: 60px ;" id="null_text"></h3>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    
  </div>
</div>

<!-- header tittle -->
<div class="container">
  <div class="row">
    <h3 class="Font1 text-danger mt-3 z-index" id="body_title">Your Character Cards</h3>
    <div class="col-3">
    </div>
  </div>
</div>

<!-- body cards -->
<div class="container">
  <div class="row mb-5">
    <?php foreach($char as $row) : ?>
    <div class="card m-3 mr-3 bg-dark z-index" id="card_body">
      <div class="card-body Font1">
        
        <!-- Delete Button -->
        <a class="close " onclick="return confirm('want delete this charater')" aria-label="Close" id="button_delete" href="index.php?id_char='<?php echo base64_encode($row['id_char']); ?>'">
          <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-contract" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M3.646 13.854a.5.5 0 0 0 .708 0L8 10.207l3.646 3.647a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 0 0 0 .708zm0-11.708a.5.5 0 0 1 .708 0L8 5.793l3.646-3.647a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 0-.708z"/>
          </svg>
        </a>
        
        <!-- Card -->
        <a href="edit.php?edit=<?php echo $row['id_char']; ?>" class="card-title text-light ml-2" id="card_header" ><?=$row['nama'];?></a>
        <ul style="list-style: none;">
          <li>
            <img  id="CardImg" class="mx-auto" src="asset/image/bg_card2.png" alt="not found" >
            <p class=" text-light">Role : <?=$row['role'];?></p>
          </li>
          <li>
            <p class="text-light">Weapon: <?=$row['senjata'];?></p>
          </li>
          <li>
            <p class="text-light">Gender : <?=$row['gender'];?></p>
          </li>
        </ul>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</div>




<!-- font awesome -->
<script src="https://kit.fontawesome.com/6dcdfb065e.js" crossorigin="anonymous"></script>
<!-- Option 1: jQuery and Bootstrap Bundle  -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>


</body>
</html>
<!-- this web site is create by akbar kurnia -->