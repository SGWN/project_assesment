<?php
//=================== edit form

require 'inc/connection.php';

$editData = $db->query("SELECT * FROM data_table WHERE id_char=".$_GET['edit']);
$edit = $editData->fetchAll();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
 <!-- css from asset -->
  <link rel="stylesheet" href="asset/css/Style.css">
  <link rel="stylesheet" href="asset/css/loading.css">

    <title>Update</title>
</head>
<body>


  <!-- background image -->
  <div class="fullscreen-bg">
    <img src="asset/image/bg_3.png" id="BackgroundImageEdit" alt="not Found">
  </div>

  <div class="container">
    <div class="row">
      <!-- Form Edit -->
      <form class="col-4 Font1 text-light ml-4 mt-4" action="index.php" method="POST" >
        <div class= "from-group">
          <div class="form-group mx-auto">
            <!-- Name edit -->
            <label for="InputName" >Name</label>
            <input type="text" name="update_name" class="form-control" value="<?php echo $edit[0]['nama']; ?>">
          </div>
          <!-- role edit -->
          <div class="from-group">
          <label for="SelectRole">role</label>
          <select name="update_role" class="form-control" value="<?php echo $edit[0]['role'];?>" autocomplete="off">

            <?php
            $role_char=['Mage','Archer','Assasin','Warrior','Support','Tank'];
            ?>

            <?php foreach ($role_char as $key):?>
              <?php if($key==$edit[0]['role']):?>
                <option selected><?php echo $key;?></option>
              <?php else:?>
                <option><?php echo $key;?></option>
              <?php endif;?>
              <?php endforeach;?>
          </select> 
        </div>
          
        <!-- weapon edit -->
      <div class="form-group">
          <label for="SelectWeapon">Weapon</label>
          <select name="update_weapon" class="form-control" value="<?php echo $edit[0]['senjata'];?>" autocomplete="off">
         <?php
            $weapon_char=["Sword","Bow","katana","Staff","Knife","Shield"];
            ?>

            <?php foreach($weapon_char as $key):?>
              <?php if($key==$edit[0]['senjata']):?>
              <option selected><?php echo $key;?></option>
              <?php else:?>
                <option><?php echo $key;?></option>
              <?php endif;?>
              <?php endforeach;?>
            </select>
              </div>    
            
              <!-- gender edit -->
      <div class="form-group">
          <label for="SelectGender">Gender</label>
          <select name="update_gender" class="form-control" value="<?php echo $edit[0]['gender'];?>" autocomplete="off">
         <?php
            $weapon_char=["Male","Female"];
            ?>

            <?php foreach($weapon_char as $key):?>
              <?php if($key==$edit[0]['gender']):?>
              <option selected><?php echo $key;?></option>
              <?php else:?>
                <option><?php echo $key;?></option>
              <?php endif;?>
              <?php endforeach;?>
            </select>
              </div>    
            
              <!-- id character -->
        <input type="hidden" name="update_char" value="<?php echo $edit[0]['id_char']; ?>">
       
        <!-- button submit into index.php -->
        <button type="submit" class="btn btn-danger" name="edit" id="button_edit">Change</button>
      </form>
    </div>
    <div class="col-6">
      <h1 style="padding-top:30px; padding-left:70px; font-size:100px" class="Font1 text-danger" > Edit Your Character </h1>
    </div>
  </div>
  
  <!-- loading if submit -->
  <div id="loading">
    <div class="lds-ripple" id="loading-image"><div></div><div></div></div>
      </div>

     <!-- Option 1: jQuery and Bootstrap Bundle  -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script>
  $(document).ready(function() {
    $('#loading').hide();
    $('#button_edit').click(function() {
       $('#loading').show();
    });
  });
  </script>
</body>
</html>