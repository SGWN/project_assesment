<?php

/* this database is named data_table and contains table
-id_char for id character
-role for role character
-senjata for weapon character
-gender for gender character
*/
try {
    $db = new PDO("mysql:host=localhost;dbname=data","root","",[PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION]);
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

// select all from database
$data=$db->query("select * from data_table");
$char=$data->fetchAll();