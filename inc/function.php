<?php

// Add Character
function addCharacter($name,$role,$weapon,$gender)
{
    include 'connection.php';


    $sql = 'INSERT INTO data_table(nama, role, senjata, gender) VALUES(?, ?, ?, ?)';

    try{
        $result = $db->prepare($sql);
        $result->bindValue(1, $name, PDO::PARAM_STR);
        $result->bindValue(2, $role, PDO::PARAM_STR);
        $result->bindValue(3, $weapon, PDO::PARAM_STR);
        $result->bindValue(4, $gender, PDO::PARAM_STR);
       
        $result->execute();
        header("location:../index.php");
    } catch (Exception $e){
        echo "Error!: " . $e->getMessage() . "<br />";
        return false;
    }
    return true;
}

// Delete Character
function deleteCharacter($id){
    include 'connection.php';
    
    $sql = 'DELETE FROM data_table WHERE id_char= ?';

    try {
        $result = $db->prepare($sql);
        $result->bindValue(1, $id, PDO::PARAM_STR);
        $result->execute();
        header("location:../index.php");
    }catch (Exception $e) {
        echo "Error!: " . $e->getMessage() . "<br />";
        return false;
    }
    return true;
    
}

 // Update Character
function updateCharacter($name, $role, $weapon, $gender, $id_char)
{
    include 'connection.php';

    $sql = 'UPDATE data_table SET nama= ?, role= ?,senjata= ?,gender= ? WHERE id_char= ?';
   try{
    $result=$db->prepare($sql);
    $result->bindValue(1, $name, PDO::PARAM_STR);
    $result->bindValue(2, $role, PDO::PARAM_STR);
    $result->bindValue(3, $weapon, PDO::PARAM_STR);
    $result->bindValue(4, $gender, PDO::PARAM_STR);
    $result->bindValue(5, $id_char, PDO::PARAM_INT);
    
    $result->execute();
    header("location:../index.php");
   }
   catch (Exception $e){
       echo "Error!: " . $e->getMessage() . "<br />";
       return false;
   }
   return true;
}

// Search character
function loopSearch($query)
{
    include 'connection.php';

    $result = $db->query($query);
    $rows=[];
    while($row = $result->fetch(PDO::FETCH_ASSOC))
    {
        $rows[]=$row;
    }
    return $rows;
}

// Keyword validation
function search($keyword)
{
    $sql = "SELECT * FROM data_table WHERE nama LIKE '%$keyword%' OR role LIKE '%$keyword%' OR senjata LIKE '%$keyword%' OR gender LIKE '%$keyword%' ";

    return loopSearch($sql);
}


// logical CRUD

 //delete character logic
if (isset($_GET['id_char'])) {
    deleteCharacter(base64_decode($_GET['id_char']));
  }else{

    //create character logic
  if (isset($_POST['sub-create'])){
    addCharacter($_POST['cre-name'],$_POST['cre-role'],$_POST['cre-weapon'],$_POST['cre-gender']);
  }
  }
  
  //update character logic
  if (isset($_POST['edit'])){
    updateCharacter($_POST['update_name'],$_POST['update_role'],$_POST['update_weapon'],$_POST['update_gender'],$_POST['update_char']);
  }
  
  //search logic
  $tempARR=[];
  if (isset($_POST['search_char'])){
    $tempARR = search($_POST['search']);
    if(!empty($tempARR)){
      $char = $tempARR;
    }elseif (empty($tempARR)){
      $error = $_POST['search']." has not found";
    }
  }